﻿open System.IO

// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

[<EntryPoint>]
let main argv = 
    for file in argv do
        let lines = File.ReadAllLines(file)
        let signatures =
            seq {
                for lineno, line in Seq.indexed lines do
                    yield
                        try
                            Signatures.fromString (line + "\n")
                            |> function
                                | Ok x -> x
                                | Error msg ->
                                    printfn "Error at line %d: %s" (lineno + 1) msg
                                    None
                        with
                        | exc ->
                            printfn "Uncaught exception when processing line %d: %s" (lineno + 1) exc.Message
                            None
            }
            |> Seq.choose id
        let classes =
            Classes.mkClassDefinitions signatures
            |> List.ofSeq
        let hierarchy = Classes.mkTopNamespace classes
        let layout = Printing.LayoutEngine.Compute(hierarchy)
        let repr = Printing.print layout
        printfn "%s" repr
        let outFileName =
            Path.Combine(
                Path.GetDirectoryName(file),
                Path.GetFileNameWithoutExtension(file)) + ".h"
        File.WriteAllText(outFileName, repr)

    0 // return an integer exit code
