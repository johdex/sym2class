﻿module AbstractSyntax

type Access = Public | Private | Protected | Default

type CallConvention = CDecl | DefaultCC

type PointerSize = Unspecified | Ptr64

type Kind =
    | Class
    | Struct
    | Enum
    | Primitive

type CType =
    { Path: PathPart list
      Head : PathPart
      Kind: Kind
    }
and RefType =
    | IntConstant of int
    | Direct of CType
    | Const of RefType
    | Pointer of RefType * PointerSize
    | Reference of RefType * PointerSize
and PathPart =
    { TypeName : string // Or identifier
      TemplateArgs : RefType list }

let (|PrimitiveType|_|) x =
    match x with
    | Direct { Path = []; Head = { TypeName = s; TemplateArgs = [] }; Kind = Primitive } -> Some s
    | _ -> None

type FunctionName =
    | Regular of PathPart
    | Operator of string

type Function =
    { Access: Access
      IsVirtual: bool
      CallConvention: CallConvention
      ReturnType: RefType
      Path: PathPart list
      Name: FunctionName
      ArgTypes: RefType list
      IsConst: bool
      IsStatic: bool
      PointerSize: PointerSize
    }

type Variable =
    { Access: Access
      IsStatic: bool
      VariableType: RefType
      Path: PathPart list
      Name: string
      PointerSize: PointerSize
    }

type Destructor =
    { Access: Access
      IsVirtual: bool
      CallConvention: CallConvention
      Path: PathPart list
      Head: PathPart
      PointerSize: PointerSize
    }

type Constructor =
    { Access: Access
      CallConvention: CallConvention
      Path: PathPart list
      Head: PathPart
      ArgTypes: RefType list
      PointerSize: PointerSize
    }

type Signature =
    | Function of Function
    | Constructor of Constructor
    | Destructor of Destructor
    | Variable of Variable
with
    member this.Path =
        match this with
        | Function f -> f.Path
        | Constructor c -> c.Path
        | Destructor d -> d.Path
        | Variable v -> v.Path

let splitLast xs =
    xs
    |> List.rev
    |> function
        | last :: xs ->
            List.rev xs, last
        | [] ->
            invalidArg "xs" "Must not be empty"

#if DEBUG
module Debug =
    let mutable inputString : string = ""

let dbg (state : Microsoft.FSharp.Text.Parsing.IParseState) s =
    let start, finish = state.ResultRange
    let substr =
        try
            Debug.inputString.[start.AbsoluteOffset .. finish.AbsoluteOffset - 1]
        with
        | _ -> "<unset>"
    printfn "%s >> %s " substr s
#else
let dbg _ _ = ()
#endif