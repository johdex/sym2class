﻿module Classes

open AbstractSyntax

type ClassDefinition =
    { Path : PathPart list
      Name : string
      TemplateArgs : RefType list
      IsStruct : bool
      Constructors : Constructor list
      Methods : Function list
      Destructor : Destructor option
      Variables : Variable list
    }
with
    static member Create(path, name, targs) =
        { Path = path
          Name = name
          TemplateArgs = targs
          IsStruct = false
          Constructors = []
          Methods = []
          Destructor = None
          Variables = []
        }

    member this.AddMember(signature : Signature) =
        match signature with
        | Function f -> { this with Methods = f :: this.Methods }
        | Constructor f -> { this with Constructors = f :: this.Constructors }
        | Destructor f -> { this with Destructor = Some f }
        | Variable v -> { this with Variables = v :: this.Variables }

let isMember s =
    match s with
    | Function { PointerSize = Unspecified } -> false // Non-member function
    | Function _ -> true // Member function
    | Variable { IsStatic = true } -> true // Static member variable
    | Variable _ -> false // Non-static member variables cannot be exposed in DLLs: non-member variable
    | Destructor _ -> true
    | Constructor _ -> true

let mkClassDefinitions (signatures : Signature seq) =
    signatures
    |> Seq.filter isMember
    |> Seq.groupBy (fun s -> s.Path)
    |> Seq.map (fun (path, signatures) ->
        match List.rev path with
        | type_ :: rPath ->
            let cls = ClassDefinition.Create(List.rev rPath, type_.TypeName, type_.TemplateArgs)
            signatures
            |> Seq.fold (fun (cls : ClassDefinition) signature -> cls.AddMember(signature)) cls
        | [] ->
            failwith "member lacks a containing type"
    )

let rec internal removePrefix prefix path =
    match prefix, path with
    | [], _ -> Some path
    | _ :: _, [] -> None
    | x :: xs, y :: ys ->
        if x <> y then
            None
        else
            removePrefix xs ys

let internal listMap func prefix xs =
    xs
    |> List.map (fun x -> func(x, prefix))

// Path shortening
type PathShortening() =
    static member Shorten(pathPart : PathPart, prefix : PathPart list) =
        { pathPart with
            TemplateArgs = pathPart.TemplateArgs |> listMap PathShortening.Shorten prefix
        }

    static member Shorten(path : PathPart list, prefix : PathPart list) =
        path
        |> removePrefix prefix
        |> Option.defaultValue path
        |> listMap PathShortening.Shorten prefix

    static member Shorten(ctyp : CType, prefix : PathPart list) =
        { ctyp with
            Path = PathShortening.Shorten(ctyp.Path, prefix)
            Head = PathShortening.Shorten(ctyp.Head, prefix) }

    static member Shorten(typ : RefType, prefix) =
        match typ with
        | IntConstant _ -> typ
        | Direct ctype -> Direct(PathShortening.Shorten(ctype, prefix))
        | Const typ -> Const(PathShortening.Shorten(typ, prefix))
        | Pointer(typ, sz) -> Pointer(PathShortening.Shorten(typ, prefix), sz)
        | Reference(typ, sz) -> Reference(PathShortening.Shorten(typ, prefix), sz)

    static member Shorten(args : RefType list, prefix) =
        args
        |>  listMap PathShortening.Shorten prefix

    static member Shorten(func : Function, prefix : PathPart list) =
        { func with
            ReturnType = PathShortening.Shorten(func.ReturnType, prefix)
            Path = PathShortening.Shorten(func.Path, prefix)
            ArgTypes = PathShortening.Shorten(func.ArgTypes, prefix)
        }

    static member Shorten(constructor : Constructor, prefix) =
        { constructor with
            Path = PathShortening.Shorten(constructor.Path, prefix)
            ArgTypes = PathShortening.Shorten(constructor.ArgTypes, prefix)
        }

    static member Shorten(destructor : Destructor, prefix) =
        { destructor with
            Path = PathShortening.Shorten(destructor.Path, prefix)
        }

    static member Shorten(v : Variable, prefix) =
        { v with
            Path = PathShortening.Shorten(v.Path, prefix)
        }

    static member Shorten(klass : ClassDefinition, prefix) =
        { klass with
            Path = PathShortening.Shorten(klass.Path, prefix)
            TemplateArgs = PathShortening.Shorten(klass.TemplateArgs, prefix)
            Constructors = klass.Constructors |> listMap PathShortening.Shorten prefix
            Variables = klass.Variables |> listMap PathShortening.Shorten prefix
            Methods = klass.Methods |> listMap PathShortening.Shorten prefix
            Destructor = klass.Destructor |> Option.map(fun d -> PathShortening.Shorten(d, prefix))
        }

type ClassDefinition with
    member this.ShortenPaths(prefix) =
        PathShortening.Shorten(this, prefix)

// Build namespaces and nested classes
type TypeContainerKind =
    | Namespace of string
    | Class of ClassDefinition

type TypeContainer =
    { Kind : TypeContainerKind
      Types : TypeContainer list }

let rec fillTypes (path : PathPart list) (parent : TypeContainer) (classes : ClassDefinition list) =
    let children, descendants =
        classes
        |> List.partition (fun klass -> klass.Path = [])
    let subContainers =
        children
        |> Seq.map (fun klass -> { TypeName = klass.Name; TemplateArgs = klass.TemplateArgs }, { Kind = Class { klass.ShortenPaths(path) with Path = path }; Types = [] })
        |> Map.ofSeq
    let subContainers =
        descendants
        |> List.groupBy (fun descendant -> List.head descendant.Path)
        |> List.fold (fun subContainers (containerId, descendants) ->
            let subContainer =
                match Map.tryFind containerId subContainers with
                | None ->
                    match containerId with
                    | { TypeName = name; TemplateArgs = [] } -> Namespace name
                    | { TypeName = name; TemplateArgs = tplArgs } -> Class(ClassDefinition.Create(path, name, tplArgs).ShortenPaths(path))
                    |> fun kind -> { Kind = kind; Types = []}
                | Some container -> container
            let subContainer =
                descendants
                |> List.map (fun klass -> { klass with Path = List.tail klass.Path })
                |> fillTypes (path @ [containerId]) subContainer
            subContainers.Add(containerId, subContainer)) subContainers
        |> Map.toSeq
        |> Seq.map snd
        |> List.ofSeq
    { parent with Types = subContainers }

let mkTopNamespace classes = fillTypes [] { Kind = Namespace ""; Types = [] } classes