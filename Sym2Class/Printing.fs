﻿module Printing

open Microsoft.FSharp.Text.StructuredFormat
open Microsoft.FSharp.Text.StructuredFormat.LayoutOps

open AbstractSyntax
open Classes

/// Same as operator ^^, but with higher precedence.
// I find it a bit counter-intuitive that a ^^ b @@ c is parsed as a ^^ (b @@ c)
let ( ** ) (l : Layout) (r : Layout) =
    l ^^ r

/// Force a space between two unbreakable terms
let ( *-* ) (l : Layout) (r : Layout) =
    if isEmptyL l then
        r
    elif isEmptyL r then
        l
    else
        l ** sepL " " ** r

/// List of terms separated by spaces
// The original in StructuredFormat doesn't seem to enforce this
let spaceListL xs =
    xs
    |> List.fold ( *-* ) emptyL

type LayoutEngine() =
    static member Compute(access : AbstractSyntax.Access) =
        match access with
        | Public -> wordL "public:"
        | Protected -> wordL "protected:"
        | Private -> wordL "private:"
        | Default -> emptyL

    static member Compute(kind : AbstractSyntax.Kind) =
        match kind with
        | Kind.Class -> wordL "class"
        | Struct -> wordL "struct"
        | Enum -> wordL "enum"
        | Primitive -> emptyL

    static member Compute(ctype : AbstractSyntax.CType) =
        let pathHeadLayout =
            ctype.Path @ [ctype.Head]
            |> List.map LayoutEngine.Compute
            |> sepListL (sepL "::")
        LayoutEngine.Compute(ctype.Kind) *-* pathHeadLayout

    static member Compute(refType : AbstractSyntax.RefType) =
        match refType with
        | IntConstant n -> wordL (string n)
        | Direct t -> LayoutEngine.Compute(t)
        | Const t -> LayoutEngine.Compute(t) *-* wordL "const"
        | Pointer (t, _) -> LayoutEngine.Compute(t) ** sepL "*"
        | Reference (t, _) -> LayoutEngine.Compute(t) ** sepL "&"

    static member Compute(part : AbstractSyntax.PathPart) =
        let argsLayout =
            match part.TemplateArgs with
            | [] -> emptyL
            | args -> sepL "<" ** (part.TemplateArgs |> List.map LayoutEngine.Compute |> commaListL) ** leftL ">"
        wordL part.TypeName ** argsLayout

    static member Compute(path : PathPart list) =
        path
        |> List.map LayoutEngine.Compute
        |> sepListL (sepL "::")

    static member Compute(funName : AbstractSyntax.FunctionName) =
        match funName with
        | Regular part -> LayoutEngine.Compute(part)
        | Operator op -> wordL <| sprintf "operator%s" op

    static member Compute(func : AbstractSyntax.Function) =
        LayoutEngine.Compute(func.Access) --
            spaceListL [
                if func.IsStatic then
                    yield wordL "static"
                if func.IsVirtual then
                    yield wordL "virtual"
                yield LayoutEngine.Compute(func.ReturnType)
                yield LayoutEngine.Compute(func.Name) ** bracketL(func.ArgTypes |> List.map LayoutEngine.Compute |> commaListL)
                if func.IsConst then
                    yield wordL "const"
            ] ** sepL ";"

    static member Compute(v : AbstractSyntax.Variable) =
        LayoutEngine.Compute(v.Access) --
            spaceListL [
                if v.IsStatic then yield wordL "static"
                yield LayoutEngine.Compute(v.VariableType)
                yield LayoutEngine.Compute({ TypeName = v.Name; TemplateArgs = [] })
            ] ** sepL ";"

    static member Compute(destructor : AbstractSyntax.Destructor) =
        LayoutEngine.Compute(destructor.Access) --
            spaceListL [
                if destructor.IsVirtual then yield wordL "virtual"
                yield sepL "~" ** LayoutEngine.Compute(destructor.Head) ** bracketL(emptyL)
            ] ** sepL ";"

    static member Compute(constructor : AbstractSyntax.Constructor) =
        LayoutEngine.Compute(constructor.Access) --
            spaceListL [
                yield LayoutEngine.Compute(constructor.Head) ** bracketL(constructor.ArgTypes |> List.map LayoutEngine.Compute |> commaListL)
            ] ** sepL ";"

    static member Compute(signature : AbstractSyntax.Signature) =
        match signature with
        | Function f -> LayoutEngine.Compute(f)
        | Constructor c -> LayoutEngine.Compute(c)
        | Destructor d -> LayoutEngine.Compute(d)
        | Variable v -> LayoutEngine.Compute(v)

    static member Compute(classDef : Classes.ClassDefinition, ?nestedTypes : Layout) =
        let templateDecl = if classDef.TemplateArgs = [] then emptyL else wordL "template<>"
        let typeWord = if classDef.IsStruct then wordL "struct" else wordL "class"
        let nameWord = LayoutEngine.Compute({ TypeName = classDef.Name; TemplateArgs = classDef.TemplateArgs })
        let mkBlock layoutOf (vs : 'T list) =
            vs
            |> List.map layoutOf
            |> List.fold (@@) emptyL
        let varBlock = classDef.Variables |> mkBlock LayoutEngine.Compute
        let constructorBlock = classDef.Constructors |> mkBlock LayoutEngine.Compute
        let methodsBlock = classDef.Methods |> mkBlock LayoutEngine.Compute
        let destructor = classDef.Destructor |> Option.map LayoutEngine.Compute |> Option.defaultValue emptyL
        templateDecl ++ typeWord ++ nameWord @@ (
            wordL "{" @@-
            defaultArg nestedTypes emptyL @@ varBlock @@ constructorBlock @@ methodsBlock @@ destructor
        ) @@ wordL "};"

    static member Compute(container : Classes.TypeContainer) : Layout =
        let content =
            container.Types
            |> List.map LayoutEngine.Compute
            |> List.fold (@@) emptyL
        match container.Kind with
        | Namespace "" ->
            content
        | Namespace name ->
            wordL "namespace" ++ wordL name ++ (leftL "{" @@- content) @@ rightL "};"
        | Class classDef ->
            LayoutEngine.Compute(classDef, content)

open Microsoft.FSharp.Text.StructuredFormat.Display

let print (l : Layout) =
    use s = new System.IO.StringWriter()
    output_layout FormatOptions.Default s l
    string s