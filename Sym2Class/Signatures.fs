﻿module Signatures

open Microsoft.FSharp.Text.Lexing

let fromStringInternal f s =
#if DEBUG
    AbstractSyntax.Debug.inputString <- s
#endif
    let lexbuf = LexBuffer<char>.FromString s
    let signature =
        try
            f Lexer.tokenstream lexbuf
            |> Ok
        with
        | err -> Error err.Message
    signature

let fromString s =
    fromStringInternal Parser.signature s
