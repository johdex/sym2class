﻿module Tests

open NUnit.Framework
open System.IO

open Microsoft.FSharp.Text.StructuredFormat.Display

open AbstractSyntax
open Signatures
open Classes
open Printing

let sid s =
    { TypeName = s; TemplateArgs = [] }

[<Test>]
let constructorTest1 () =
    let res = fromString "public: __cdecl LocationManagerSpace::FullSquadronInfo::FullSquadronInfo(struct LocationManagerSpace::FullSquadronInfo const & __ptr64) __ptr64\n"
    match res with
    | Ok(Some(Constructor constructor)) ->
        Assert.AreEqual(Public, constructor.Access)
        Assert.AreEqual(CDecl, constructor.CallConvention)
        Assert.AreEqual([sid "LocationManagerSpace"; sid "FullSquadronInfo"], constructor.Path)
        Assert.AreEqual(sid "FullSquadronInfo", constructor.Head)
        match constructor.ArgTypes with
        | [] -> Assert.Fail("No arg types")
        | _ :: _ :: _ -> Assert.Fail("Too many arg types")
        | [ Reference(Const(Direct typ), Ptr64) ] ->
            Assert.AreEqual([sid "LocationManagerSpace"], typ.Path)
            Assert.AreEqual(sid "FullSquadronInfo", typ.Head)
            Assert.AreEqual(Struct, typ.Kind)
        | _ -> Assert.Fail("Wrong arg type")
    | Ok(Some(_)) -> Assert.Fail("Expected a constructor")
    | Ok(None) -> Assert.Fail("Expected a signature")
    | Error err -> Assert.Fail("Error: " + err)

let assertAccepts res =
    match res with
    | Ok(Some _) -> ()
    | Ok(None) -> Assert.Fail("Expected a signature")
    | Error err -> Assert.Fail("Error: " + err)

[<Test>]
let parserAcceptsConstructors() =
    let res = fromString "public: __cdecl CCells<int,unsigned char>::CCells<int,unsigned char>(void) __ptr64\n"
    assertAccepts res
    match res with
    | Ok(Some(Constructor { Access = Public; CallConvention = CDecl; Path = [_]; Head = { TypeName = "CCells" }; ArgTypes = [ Direct { Kind = Primitive }]})) ->
        ()
    | _ ->
        Assert.Fail("Not the expected constructor signature")

    let res = fromString "public: __cdecl CCells<class MissionManagerSpace::CMRoadSegment * __ptr64,unsigned char>::CCells<class MissionManagerSpace::CMRoadSegment * __ptr64,unsigned char>(void) __ptr64\n"
    assertAccepts res

    let res = fromString "public: __cdecl common::CGraphNode<double>::CGraphNode<double>(class common::CGraphNode<double> const & __ptr64) __ptr64\n"
    assertAccepts res

[<Test>]
let parserAcceptsMethods() =
    let res = fromString "public: virtual void __cdecl MissionManagerSpace::MCU_TR_AI_Poi::saveToFileBinary(struct _iobuf * __ptr64)const __ptr64\n"
    assertAccepts res

    let res = fromString "public: void __cdecl MissionManagerSpace::MPlane::setAILevel(enum MissionManagerSpace::AILEVEL,bool * __ptr64) __ptr64\n"
    assertAccepts res

    let res = fromString "public: void __cdecl MissionManagerSpace::MCU_TR_AnimationOperator::setConfig(class std::basic_string<char,struct std::char_traits<char>,class std::allocator<char> >) __ptr64\n"
    assertAccepts res

[<Test>]
let parserAcceptsDestructors() =
    let res = fromString "public: virtual __cdecl CCells<int,unsigned char>::~CCells<int,unsigned char>(void) __ptr64\n"
    assertAccepts res

[<Test>]
let parserAcceptsOperatorEqual() =
    let res = fromString "public: struct boost::spirit::impl::object_with_id<struct boost::spirit::impl::grammar_tag,unsigned __int64> & __ptr64 __cdecl boost::spirit::impl::object_with_id<struct boost::spirit::impl::grammar_tag,unsigned __int64>::operator=(struct boost::spirit::impl::object_with_id<struct boost::spirit::impl::grammar_tag,unsigned __int64> const & __ptr64) __ptr64\n"
    assertAccepts res

[<Test>]
let parserAcceptsOperatorOrder() =
    let res = fromString "bool __cdecl LocationManagerSpace::operator<(struct LocationManagerSpace::LDate const & __ptr64,struct LocationManagerSpace::LDate const & __ptr64)\n"
    assertAccepts res

[<Test>]
let parserAcceptsVFTable() =
    let res = fromString "const CCells<int,unsigned char>::`vftable'\n"
    match res with
    | Ok(None) -> ()
    | Ok(Some _) -> Assert.Fail("Did not expect a signature")
    | Error err -> Assert.Fail("Error: " + err)

[<Test>]
let parserAcceptsIntTemplateArgs() =
    let res = fromString "public: __cdecl common::CGraphNode<double>::CGraphNode<double>(int,class Math::Vector<2,double>) __ptr64\n"
    assertAccepts res

[<Test>]
let parserAcceptsConstEnum() =
    let res = fromString "public: __cdecl MissionManagerSpace::CDBSegments::CDBSegments(class std::map<enum MissionManagerSpace::RoadType,class MissionManagerSpace::CMRoadArray * __ptr64,struct std::less<enum MissionManagerSpace::RoadType>,class std::allocator<struct std::pair<enum MissionManagerSpace::RoadType const,class MissionManagerSpace::CMRoadArray * __ptr64> > > * __ptr64) __ptr64\n"
    assertAccepts res

[<Test>]
let parserAcceptsWCharT() =
    let res = fromString "public: int __cdecl MissionManagerSpace::MultiLanguageLocalizationTable::addLocalizedWString(class std::basic_string<wchar_t,struct std::char_traits<wchar_t>,class std::allocator<wchar_t> > const & __ptr64) __ptr64\n"
    assertAccepts res

[<Test>]
let extractsClasses() =
    let here = System.Reflection.Assembly.GetAssembly(typeof<Signature>).Location
    let here = Path.GetDirectoryName(here)
    let lines = File.ReadAllLines(Path.Combine(here, "Data", "MissionManager.txt"))
    let signatures =
        seq {
            for lineno, line in Seq.indexed lines do
                yield
                    try
                        fromString (line + "\n")
                        |> function
                            | Ok x -> x
                            | Error msg ->
                                printfn "Error at line %d: %s" (lineno + 1) msg
                                None
                    with
                    | exc ->
                        printfn "Uncaught exception when processing line %d: %s" (lineno + 1) exc.Message
                        None
        }
        |> Seq.choose id
    let classes = mkClassDefinitions signatures
    Assert.IsNotEmpty classes

[<Test>]
let printType() =
    let ast = fromStringInternal Parser.ctype "class ns1::ns2::cl<enum e, 6, class c<int> >::t<1, unsigned long int>\n"
    match ast with
    | Ok {
            Path = [
                { TypeName = "ns1" }
                { TypeName = "ns2" }
                { TypeName = "cl"
                  TemplateArgs = [ Direct { Kind = Enum }; IntConstant 6; Direct { Kind = Kind.Class } ]
                }
            ]
            Head = { TypeName = "t"
                     TemplateArgs = [ IntConstant 1; Direct { Kind = Primitive; Head = { TypeName = "unsigned long int" } } ]
                   }
            Kind = Kind.Class
        } -> ()
    | Error msg ->
        Assert.Fail (sprintf "Failed to parse: '%s'" msg)
    | _ ->
        Assert.Fail "Failed to parse CType"
    match ast with
    | Ok ast ->
        let layout = LayoutEngine.Compute(ast)
        let s = print layout
        Assert.AreEqual("class ns1::ns2::cl<enum e, 6, class c<int>>::t<1, unsigned long int>", s)
    | _ ->
        ()

[<Test>]
let nestedShortening() =
    let res = fromString "public: void __cdecl MissionManagerSpace::MissionManager::doSomething(class std::ptr<class MissionManagerSpace::MissionObject>) __ptr64\n"
    match res with
    | Error s -> Assert.Fail(s)
    | Ok None -> Assert.Fail("No result")
    | Ok (Some ast) ->
        let classes = mkClassDefinitions [ast] |> List.ofSeq
        let ns = mkTopNamespace classes
        match ns.Types with
        | [ { Types = [ { Kind = Class klass } ] } ] ->
            match klass.Methods with
            | [ method ] ->
                match method.ArgTypes with
                | [ Direct { Head = ptr } ] ->
                    match ptr.TemplateArgs with
                    | [ Direct arg ] ->
                        Assert.IsEmpty(arg.Path)
                    | _ ->
                        Assert.Fail("Wrong template argument")
                | _ ->
                    Assert.Fail("Wrong method argument types")
            | _ ->
                Assert.Fail("Wrong number of methods")
        | _ ->
            Assert.Fail("Wrong type hierarchy")
        let layout = LayoutEngine.Compute(ns)
        let s = print layout
        printfn "%s" s